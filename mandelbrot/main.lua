local function abs(i) if (i < 0) then return i*-1 else return i end end
local insert = table.insert
local setColor = love.graphics.setColor
local gpoints = love.graphics.points

local points_limit = 360000
local amp_prop = 0.1
local amp_prop = amp_prop/2
--local amp_prop = 0.5
local frame_delay = 0.3
local width = 300
local height = 300
local y_start, y_end = -2.1, 2.1
local y_step = (abs(y_start)+abs(y_end))/height

local x_start, x_end = -2.1, 2.1
local x_step = (abs(x_start)+abs(x_end))/width

local iterations = 100
points = {}


local colors = {}
local amp_prop = (1-amp_prop)/2
for g=0,1,0.1
do
  for r=0,1,0.1
  do
    for b=0,1,0.1
    do
      insert(colors, {r,g,b})
    end
  end
end



local colors_counts = #colors

function love.conf(t)
  t.console = true
  t.title = "Mandelbrot Explorer"
end

local function check_inf(x, y, iter, iter_max, zr, zi)
  if ( iter > iter_max  or zi * zi + zr * zr >= 4 )
  then
    return iter
  else
    return check_inf(x, y, iter+1, iter_max, zr*zr-zi*zi+x, 2*zr*zi+y)
  end
end

local check_inf = check_inf

local function debug_print()
  cur_x, cur_y = love.mouse.getPosition( )
  print('mouse: ',(x_step*cur_x)+x_start..'-'..(y_step*cur_y)+y_start,false,cur_x, cur_y)
  print('x start:    '..string.format('%.99f',x_start))
  print('x end:      '..string.format('%.99f',x_end))
  print('x diff:     '..string.format('%.99f', x_start-x_end))
  print('x step:      '..string.format('%.99f', x_step))
  print('width:       '..width)
  print('y start:    '..string.format('%.99f', y_start))
  print('y end:       '..string.format('%.99f', y_end))
  print('y diff:     '..string.format('%.99f', y_start-y_end))
  print('y step:      '..string.format('%.99f', y_step))
  print('height:      '..height)
  print('iterations:  '..iterations)
  print('points: '..#points..'\n')
end

function love.load()
  local f = love.graphics.newFont(12)
  love.graphics.setFont(f)
  love.graphics.setColor(0,0,0)
  love.graphics.setBackgroundColor(1,1,1)
  love.window.setMode( height, width, {vsync=0,msaa=0,depth=24,borderless=true, fullscreen=false} )
end


function love.update(dt)
  if love.keyboard.isDown("w")
  then
    y_start = y_start - y_step
    y_end = y_end - y_step
    debug_print()
  end

  if love.keyboard.isDown("a")
  then
    x_start = x_start - x_step
    x_end = x_end - x_step
    x_step = abs(x_start-x_end)/width
    y_step = abs(y_start-y_end)/height
    debug_print()
  end

  if love.keyboard.isDown("s")
  then
    y_start = y_start + y_step
    y_end = y_end + y_step
    debug_print()
  end


  if love.keyboard.isDown("d")
  then
    x_start = x_start + x_step
    x_end = x_end + x_step
    debug_print()
  end

  if love.keyboard.isDown("z")
  then
    iterations = iterations + 1*amp_prop
    debug_print()
  end

  if love.keyboard.isDown("x")
  then
    iterations = iterations - 1*amp_prop
    debug_print()
  end

  if love.keyboard.isDown("3")
  then
    x_avg = (abs(x_start) + abs(x_end) )/2
    y_avg = (abs(y_start) + abs(y_end) )/2
    x_start = x_avg * (-1)
    x_end   = x_avg
    y_start = y_avg * (-1)
    y_end   = y_avg
    x_step = abs(x_start-x_end)/width
    y_step = abs(y_start-y_end)/height
  end

  if love.keyboard.isDown("2")
  then
    x_abs = abs(x_start-x_end)
    y_abs = abs(y_start-y_end)

    x_center = x_start + (x_abs/2)
    y_center = y_start + (y_abs/2)

    x_start = x_center - (x_abs*(1-amp_prop))
    x_end   = x_center + (x_abs*(1-amp_prop))

    y_start = y_center - (y_abs*(1-amp_prop))
    y_end   = y_center + (y_abs*(1-amp_prop))

    x_step = abs(x_start-x_end)/width
    y_step = abs(y_start-y_end)/height
    
    -- iterations = iterations - (1/(1-amp_prop))/2
    
    debug_print()
  end

  if love.keyboard.isDown("1")
  then
    x_abs = abs(x_start-x_end)
    y_abs = abs(y_start-y_end)

    x_center = x_start + (x_abs/2) 
    y_center = y_start + (y_abs/2)

    x_start = x_center - (x_abs*amp_prop)
    x_end   = x_center + (x_abs*amp_prop)

    y_start = y_center - (y_abs*amp_prop)
    y_end   = y_center + (y_abs*amp_prop)

    x_step = abs(x_start-x_end)/width
    y_step = abs(y_start-y_end)/height
    
    -- iterations = iterations + (1/(1-amp_prop))/2

    debug_print()
  end

  if love.keyboard.isDown("q")
  then
    love.event.quit()
  end

  if love.keyboard.isDown("v")
  then
      debug_print()
      debug = true
  end


  local h = 0
  local w = 0
  for y = y_start, y_end, y_step
  do
    w = 0
    for x = x_start, x_end, x_step
    do
      points[(h*width)+w] = { w,h, check_inf(x, y , 0, iterations,  0, 0) }
      --if (h*height+w >= height*width) then print(h,height,w) end
      if (debug == true) then print(x_start, x_end, x_step) end
      if (#points > points_limit)
      then
        print(x,x_end,y,y_end)
        love.event.quit()
        break
      end
      w = w + 1
    end
    h = h + 1
  end

end

function love.draw()
  local points = points
  for index in pairs(points)
  do
    setColor(colors[points[index][3]])
    gpoints(points[index][1], points[index][2] )
  end
--  gpoints(points)
  setColor(1,0.1,0.1)
  gpoints({width/2,height/2})
  setColor(1,0.2,0.2)
  love.graphics.print("FPS: "..tostring(love.timer.getFPS( )), 10, 10)
end
